Name:           nagios-plugins-check-linux-memory
Version:        1.1.1
Release:        1%{?dist}
Summary:        Nagios/Icinga memory check plugin using /proc/meminfo

License:        GPLv2+
URL:            https://exchange.icinga.com/exchange/check_linux_memory
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

%description
I have used many memory checking programs for linux but wasn't really happy with them so I've written my own. This uses the /proc/meminfo file instead of the free command.
Currently it tracks: total, free, active, inactive, buffer and cache
It adds performance data for each of these.
I've written it to run small and clean. it seems to use about 1/2 the resources than the other checkers I've found.
There are a number of things I can add however the current version suits my needs. Perhaps if I find the time or enough people bug me about it I will put in some additional features.

%prep
curl https://exchange.icinga.com/exchange/check_linux_memory/files/496/check_linux_memory -o check_linux_memory

%install
install -m 755 -D check_linux_memory $RPM_BUILD_ROOT/%{_libdir}/nagios/plugins/check_linux_memory

%files
%defattr(-,root,root,-)
%{_libdir}/nagios/plugins/check_linux_memory

%changelog
* Wed Nov 28 2021 Jonas Svato3 <mail@jonassvatos.net> 1.1.1-1
- Initial RPM release
